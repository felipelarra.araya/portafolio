import React from 'react';
import { Fragment } from 'react';

import './styles/Navbar.css'

const Navbar = () => {
    return ( 
       <Fragment>
     
    <nav className="navbar navbar-expand-lg bg-dark">
  <a className="navbar-brand" href="#">FL</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav ml-auto">
      <li className="nav-item px-2">
        <a className="nav-link" href="#">Sobre Mí</a>
      </li>
      <li className="nav-item px-2">
        <a className="nav-link" href="#">Portafolio</a>
      </li>
      <li className="nav-item px-2">
        <a className="nav-link" href="#">Skills</a>
      </li>
      <li className="nav-item px-2">
        <a className="nav-link" href="#">Contacto</a>
      </li>
      <li className="nav-item px-2">
        <a className="nav-link" href="#">Blog</a>
      </li>
    </ul>
  </div>
</nav>
       </Fragment> 
     );
}
 
export default Navbar;