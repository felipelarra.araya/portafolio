import React from 'react';
import './App.css';
import Navbar from './Components/Navbar';
import ImagenFull from './Components/ImagenFull';
import Portfolio from './Components/Portfolio';
import About from './Components/About';


function App() {
  return (
    <div className="App">
     <Navbar/>
     <ImagenFull/>
     <About/>
     <Portfolio/>
     
    </div>
  );
}

export default App;
